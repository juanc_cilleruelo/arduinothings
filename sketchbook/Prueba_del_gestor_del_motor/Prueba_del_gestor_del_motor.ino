/* These are the pins on the Arduino */
int MotorOneLeft  = 5;
int MotorOneRight = 6;

int MotorTwoLeft  = 11;
int MotorTwoRight = 10;

int valorpote;     //Variable que recoge el valor del potenciómetro
int pwm1;          //Variable del PWM 1
int pwm2;          //Variable del PWM 2

void setup(){
   pinMode(MotorOneLeft , OUTPUT);
   pinMode(MotorOneRight, OUTPUT);
   
   pinMode(MotorTwoLeft , OUTPUT);
   pinMode(MotorTwoRight, OUTPUT);
   
}

void loop(){
  int i;
  
   Serial.begin(9600);
   //digitalWrite(MotorOneLeft , HIGH);
   //digitalWrite(MotorOneRight, LOW);
  
   //digitalWrite(MotorTwoLeft , LOW);
   //digitalWrite(MotorTwoRight, HIGH);
   
   for (i=1024; i> 870; i--){
      valorpote = i;
   
      //Como la entrada analógica del Arduino es de 10 bits, el rango va de 0 a 1023.
      //En cambio, la salidas del Arduio son de 8 bits, quiere decir, rango entre 0 a 255.
      //Por esta razón tenemos que mapear el número de un rango a otro usando este código.
      pwm1 = map(valorpote, 0, 1023, 0, 255);
      pwm2 = map(valorpote, 0, 1023, 255, 0); //El PWM 2 esta invertido respecto al PWM 1
    
      analogWrite(MotorOneLeft , pwm1);
      analogWrite(MotorOneRight, pwm2);
      
      analogWrite(MotorTwoLeft , pwm1);
      analogWrite(MotorTwoRight, pwm2);
      
      delay(10);
     
      analogWrite(MotorOneLeft , LOW);
      analogWrite(MotorOneRight, LOW);
      
      analogWrite(MotorTwoLeft , LOW);
      analogWrite(MotorTwoRight, LOW);
      delay(10);
      
      Serial.println(i);
      
   }
   
   
}
