  #define SWITCH A3
  #define LED    A4 
  
  
  void setup()
 {
   pinMode(SWITCH, INPUT);
   pinMode(LED   , OUTPUT); 
 }
 
 void loop()
{
    if (digitalRead(SWITCH)){
       digitalWrite(LED, HIGH);
    } 
    else {
       digitalWrite(LED, LOW);
    }
  
  /*delay(1000);
  digitalWrite(LED, HIGH);
  delay(1000);
  digitalWrite(LED, LOW);*/
}
