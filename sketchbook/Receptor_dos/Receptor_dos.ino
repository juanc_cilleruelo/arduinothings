
/*Ejemplo de uso

   void setup(){
     Serial.begin(9600);                  // Es necesario inicializar el puerto serie.
   }

   void loop(){
        // transmitimos cuando se pulse el boton de transmision
        Serial.println("PRENDER LED");  Las instrucciones se deben enviar con println y entre dobles comillas.
        delay(2000);                                          
        Serial.println("APAGAR LED");
        delay(1000);
   }

*/


#include <LiquidCrystal.h>
LiquidCrystal lcd(8, 9, 2, 3, 4, 5); 

/*Código creado por Castig para Taringa (14-04-2013).*/
int    numChar      =  0; //Numero ASCII del caracter.
char   incomingByte =  0; //Informacion entrante (RX)
int    cantPalabras =  1; //Cantidad de palabras que se ingresan (el espacio separa las palabras)
String palabra1     = ""; //Palabra 1 que se ingresa
String palabra2     = ""; //Palabra 2 que se ingresa
String palabra3     = ""; //PUEDEN AGREGAR TODAS LAS PALABRAS QUE QUIERAN.

boolean datosAnalizados = false; //Indica si se termino de recorrer la palabra entrante.

boolean ledPrendido     = false; //Indica si el LED se encuentra prendido.
int     pinLed          = 13;    //LED integrado en la placa Arduino.
String  BackupFirstLine;    //Saves current value of the first line of text in the LCD Display

void setup() {
    Serial.begin(9600);      
    pinMode(pinLed, OUTPUT); 
}

void loop(){
     lcd.begin(16, 2);
     
     /*showLineOnLCD("PRENDER LED"); 
     delay(1500);
     

     showLineOnLCD("APAGAR LED");  
     delay(1000);
  
     showLineOnLCD("jaaajajaja");
     delay(2000);
     showLineOnLCD("THIS IS THE END");
     delay(1000);   
 
    lcd.clear();*/
  
  
    while (Serial.available() > 0){
        numChar      = Serial.read(); //codigo numerico ASCII del caracter
        incomingByte = numChar;       //transforma el numero en su correspondiente codigo ASCII 

        //El ASCII 32 (espacio) es el delimintador de palabras.
        if (numChar == 32){
            cantPalabras++; // Si encuentra un espacio, analizamos la palabra.
        }

        /* El ASCII 13 es CARRIAGE RETURN
           El ASCII 10 es LINE FEED
           El ASCII 32 es SPACE
           Un fin de linea viene indicado por un 13 + 10.
           Mientras no sea ninguno de los ASCII anterirores...*/
        if (numChar != 13 && numChar != 10 && numChar != 32){
            if(cantPalabras == 1){
                palabra1 = palabra1 + incomingByte; //Agrega el caracter leido
            }
            if(cantPalabras == 2){
                palabra2 = palabra2 + incomingByte; //Agrega el carcater leido
            }
        }
        if (numChar == 10){
            datosAnalizados = true; //Inidica que existen datos
        }
    }    
    
    /*Si ya no hay datos para analizar, pero si hay datos analizados...*/
    if (Serial.available() <= 0 && datosAnalizados == true) {
        datosAnalizados = false; //Indico que ya no hay datos analizados, ya que los analizé previamente.

        /*Conversin a Mayusculas*/
        palabra1.toUpperCase();
        palabra2.toUpperCase();
        
        commandInterpreter(palabra1, palabra2);

        //Inicialize of Variables
        palabra1     = "";
        palabra2     = "";
        cantPalabras = 1;
        Serial.flush(); // Vacia el buffer del puerto serie.
    }
    
    if (ledPrendido == true)
    {
        digitalWrite(pinLed, HIGH);   // prendo el LED
        //lcd.println("ENCENDIDO");
    }
    else
    {
        digitalWrite(pinLed, LOW);    // apago el LED
        //lcd.println("APAGADO");
    }
}


//Función que analiza e interpreta los comandos
void commandInterpreter(String comando1, String comando2){
   //Si el primer parametro no esta vacio.
   if (comando1 != ""){
      if (comando1 == "PRENDER") {
         if (comando2 == "LED") {
            ledPrendido = true;
            showLineOnLCD("LED PRENDIDO");
         }    
         else {
            showLineOnLCD("COMANDO 2 INVALIDO");
         }
      }
        
      if (comando1 == "APAGAR") {
         if (comando2 == "LED") {
            ledPrendido = false;
            showLineOnLCD("LED APAGADO");
         }
         else {
            showLineOnLCD("COMANDO 2 INVALIDO");
         }
      }
        
      if ((comando1 != "PRENDER") && (comando1 != "APAGAR")) {
         showLineOnLCD("COMANDO 1 INVALIDO");
      }
   }
}

//Muestra una linea de texto en el display LCD
void showLineOnLCD(String ANewLine){
  
   String previousLine;
   
   previousLine = BackupFirstLine;
   lcd.clear();
   lcd.setCursor(0, 0);
   lcd.print(ANewLine);
   lcd.setCursor(0, 1);
   lcd.print(previousLine);
   BackupFirstLine = ANewLine;
   delay(1500);

}

