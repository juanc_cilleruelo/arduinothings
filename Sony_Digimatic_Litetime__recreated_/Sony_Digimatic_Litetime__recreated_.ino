#include <StepperMotor.h>

 /************************************************************************************/
 /* (cc) 2016 juanc.cilleruelo@sencille.es                                           */
 /* This project implements the functionalities to supply an old radio running again */
 /* The original model is a Radio clock Sony Digimatic Litetime.                     */
 /*                                                                                  */
 /* Module Bluetooth Sound Speaker.                                                  */
 /* This module is included inside the Sony as substitute of his original Radio.     */
 /* Takes 12v from the external feed of Arduino.                                     */
 /* Is activates by an switch that also informs arduino that is ON. (Digital Pin 8)  */
 /************************************************************************************/

#include <virtuabotixRTC.h>
#include <StepperMotor.h>

// TONES //
// Defining the relationship between note, period & frequency. 
// period is in microsecond so P = 1/f * (1E6)
 
#define  c3    7634
#define  d3    6803
#define  e3    6061
#define  f3    5714
#define  g3    5102
#define  a3    4545
#define  b3    4049
#define  c4    3816    // 261 Hz 
#define  d4    3401    // 294 Hz 
#define  e4    3030    // 329 Hz 
#define  f4    2865    // 349 Hz 
#define  g4    2551    // 392 Hz 
#define  a4    2272    // 440 Hz 
#define  a4s   2146
#define  b4    2028    // 493 Hz 
#define  c5    1912    // 523 Hz
#define  d5    1706
#define  d5s   1608
#define  e5    1517    // 659 Hz
#define  f5    1433    // 698 Hz
#define  g5    1276
#define  a5    1136
#define  a5s   1073
#define  b5    1012
#define  c6    955
 
#define  R     0      // Define a special note, 'R', to represent a rest

// MELODIES and TIMING //
//  melody[] is an array of notes, accompanied by beats[], 
//  which sets each note's relative length (higher #, longer note) 
 
// Melody 1: Star Wars Imperial March
int melody1[] = {  a4, R,  a4, R,  a4, R,  f4, R, c5, R,  a4, R,  f4, R, c5, R, a4, R,  e5, R,  e5, R,  e5, R,  f5, R, c5, R,  g5, R,  f5, R,  c5, R, a4, R};
int beats1[]  = {  50, 20, 50, 20, 50, 20, 40, 5, 20, 5,  60, 10, 40, 5, 20, 5, 60, 80, 50, 20, 50, 20, 50, 20, 40, 5, 20, 5,  60, 10, 40, 5,  20, 5, 60, 40};
 
// Melody 2: Star Wars Theme
int melody2[] = {  f4,  f4, f4,  a4s,   f5,  d5s,  d5,  c5, a5s, f5, d5s,  d5,  c5, a5s, f5, d5s, d5, d5s,   c5};
int beats2[]  = {  21,  21, 21,  128,  128,   21,  21,  21, 128, 64,  21,  21,  21, 128, 64,  21, 21,  21, 128 }; 
 
int  MAX_COUNT  = sizeof(melody1) / 2; // Melody length, for looping.
long tempo      = 10000;               // Set overall tempo
int  pause      =  1000;               // Set length of pause between notes
int  rest_count =    50;               // Loop variable to increase Rest length (BLETCHEROUS HACK; See NOTES)
 
// Initialize core variables
int  toneM     = 0;
int  beat      = 0;
long duration  = 0;
int  Note      = 0;

/********************************************************************************/

   #define SENSOR_BLUETOOTH_ON    8 
   #define SENSOR_ALARM_1        A0
   #define SENSOR_ALARM_2        A1  
   #define SENSOR_AMBIENT_LIGTH  A2  
   #define SENSOR_RESET_CLOCK    A3  
   #define SENSOR_VIBRATION      A5  
 
   #define ACTUATOR_RADIO_LIGHTS  5
   #define ACTUATOR_CLOCK_LIGHTS  6
   #define ACTUATOR_BUZZER       A4
   #define ACTUATOR_RELAY         7
       
/********************************************************************************/
/* BYJ48 Stepper motor code
   Connect : IN1 >> D12
             IN2 >> D11
             IN3 >> D10
             IN4 >> D9
   VCC ... 5V. Prefer to use external 5V Source
   In this case we are using Internal 5V because external source is 12v.
   Gnd
   http://www.instructables.com/member/Mohannad+Rawashdeh/
  */
  
  //int           Steps     = 0;
  //boolean       Direction = false;// 
  //unsigned long last_time;
  //unsigned long currentMillis;
  //int           steps_left = 409;//5;
  //long          time;
/********************************************************************************/

  
//Full constructor, just the first 4 parameters are necessary, they are the pins connected to the motor,
//the others are optional, and default to the following below
//the 5th paramater is the steps sequence, where the 1st element of the array is the number of steps
//it can have a maximum of 8 steps
//the 6th parameter is the SPR (Steps Per Rotation)
//the 7th parameter is the RPM
//the 8th parameter is the rotation orientation
  #define IN1  12
  #define IN2  11 
  #define IN3  10
  #define IN4   9

  //CustomStepper stepper(IN1, IN2, IN3, IN4, (byte[]){8, B1000, B1100, B0100, B0110, B0010, B0011, B0001, B1001}, 4075.7728395, 12, CW);
  StepperMotor motor(IN1, IN2, IN3, IN4);
/********************************************************************************/
   #define PIN_RTC_CLK 4
   #define PIN_RTC_DAT 3 //IO
   #define PIN_RTC_RST 2 //RST
   // Creation of the Real Time Clock Object
   //SCLK -> 6, I/O -> 7, CE -> 8
   virtuabotixRTC myRTC(PIN_RTC_CLK, PIN_RTC_DAT, PIN_RTC_RST);
   int CurrentMinute = 0;
/********************************************************************************/
     
       
 // Reference consts
       int AmbientLigthPoint = 800;
 
 boolean RadioLightOn;
 boolean ClockLigthOn;
 boolean AlarmSounding;
 boolean AlarmTime;
 boolean Touched;
 
 
 void setup()
 {
   pinMode(SENSOR_BLUETOOTH_ON  , INPUT);
   pinMode(ACTUATOR_RADIO_LIGHTS, OUTPUT); 
   pinMode(ACTUATOR_CLOCK_LIGHTS, OUTPUT);
   
   pinMode(SENSOR_ALARM_1  , INPUT);
   pinMode(SENSOR_ALARM_2  , INPUT);
   pinMode(SENSOR_VIBRATION, INPUT);
   
   pinMode(SENSOR_RESET_CLOCK, INPUT);
   
   digitalWrite(ACTUATOR_RADIO_LIGHTS, LOW);
   digitalWrite(ACTUATOR_CLOCK_LIGHTS, HIGH);
   
   pinMode(ACTUATOR_BUZZER, OUTPUT);
   
   Note          = 0;
   AlarmTime     = false;
   AlarmSounding = false;
   Touched       = false;
   
   /* Initialization Stepper */
   pinMode(IN1, OUTPUT); 
   pinMode(IN2, OUTPUT); 
   pinMode(IN3, OUTPUT); 
   pinMode(IN4, OUTPUT);
   
   //sets the RPM
   //stepper.setRPM(12);
   //sets the Steps Per Rotation, in this case it is 64 * the 283712/4455 annoying ger ratio
   //for my motor (it works with float to be able to deal with these non-integer gear ratios)
   //stepper.setSPR(4075.7728395);
   
   motor.setStepDuration(1);
        
   /* Initialization Timer */
   // Set the current date, and time in the following format:
   // seconds, minutes, hours, day of the week, day of the month, month, year
   myRTC.setDS1302Time(50, 00, 00, 6, 10, 1, 2016);
   
 }
 
void loop()
{
    int ActualMinute; 
    int HowManyMinutes;
    
    
    /*--------------------------------------------------------------*/
    if (digitalRead(SENSOR_RESET_CLOCK)){
      //for(int i = 0; i < 10; i++){
        AdvanceOneMinute(); 
      //}
    }
    /*--------------------------------------------------------------*/
  
    /* Puts on or off the lights of the Radio secction */
    RadioLightOn = digitalRead(SENSOR_BLUETOOTH_ON); 

    ClockLigthOn = (analogRead(SENSOR_AMBIENT_LIGTH) < AmbientLigthPoint);
    
    AlarmTime = analogRead(SENSOR_ALARM_2);
    if (!Touched){
       Touched  = !digitalRead(SENSOR_VIBRATION); 
    }
    
    if (AlarmTime){
       AlarmSounding = AlarmTime && !Touched;
    }
    else {
      Touched = false;
    }

    /*--------------------------------------------------------------*/
    
    digitalWrite(ACTUATOR_RADIO_LIGHTS, RadioLightOn); 
    digitalWrite(ACTUATOR_CLOCK_LIGHTS, ClockLigthOn);
    
    if (AlarmSounding) { 
       NextNote(Note++);
       if (Note >= MAX_COUNT) { Note = 0;}
    }
        
    //LightOn = GetPressed(INPUT_BLUETOOTH_ON);
    //digitalWrite(LIGHT_BLUETOOTH_ON, LightOn);
    
    /*---------------------------------------------------------------*/
    // This allows for the update of variables for time or accessing the individual elements.
    myRTC.updateTime();
    
    // elements as individuals
    //myRTC.dayofmonth
    //myRTC.month
    //myRTC.year
    //myRTC.hours
    //myRTC.minutes
    //myRTC.seconds

    //AdvanceOneMinute();
    
    ActualMinute = myRTC.minutes;
    if (ActualMinute > CurrentMinute){
       //HowManyMinutes = ActualMinute - CurrentMinute;
       //for (int i = 0; i < HowManyMinutes; i++) { 
          AdvanceOneMinute();   
       //}   
       CurrentMinute = ActualMinute;
    }
 }
 
 /*******************************************************/
 /* Function Sound StartWars melody                     */
 /* Every time is called, sounds a Noten                */
 /* We should mantaing a little lapse between notes     */
 /*******************************************************/
 boolean NextNote(int ANote)
 {
   // Set up a counter to pull from melody1[] and beats1[]
   toneM    = melody1[ANote];
   beat     = beats1[ANote];
   duration = beat * tempo; // Set up timing
 
   playTone(); // A pause between notes
   delayMicroseconds(pause);
 }
 
// PLAY TONE  //
// Pulse the speaker to play a tone for a particular duration
void playTone() {
   long elapsed_time = 0;
   
   if (toneM > 0) { // if this isn't a Rest beat, while the tone has 
      //  played less long than 'duration', pulse speaker HIGH and LOW
      while (elapsed_time < duration) {
         digitalWrite(ACTUATOR_BUZZER, HIGH);
         delayMicroseconds(toneM / 2);

         // DOWN
         digitalWrite(ACTUATOR_BUZZER, LOW);
         delayMicroseconds(toneM / 2);
 
         // Keep track of how long we pulsed
         elapsed_time += (toneM);
      } 
   }
   else { // Rest beat; loop times delay
      for (int j = 0; j < rest_count; j++) { // See NOTE on rest_count
         delayMicroseconds(duration);  
      }                                
   }                                 
}
  
 /*******************************************************/
 /* Function anti rebote.                               */
 /* Recibe el estadeo anterior del boton                */
 /* y devuelve el estado actual del botn "sin rebote"   */
 /*******************************************************/
 boolean GetPressed(int AButton)
 {
    /*boolean last    = digitalRead(AButton);
    delay(5);
    boolean current = digitalRead(AButton); // Read current status of the button
    
    if (last != current) {
       delay(5);                            // wait 5 miliseconds
       current = digitalRead(AButton);      // read again the status of the button
    }
    return current;, */
    
    if (digitalRead(AButton) == LOW){
       digitalWrite(ACTUATOR_RADIO_LIGHTS, LOW);
    } else {
      digitalWrite(ACTUATOR_RADIO_LIGHTS, HIGH);
    }
  }
 /*******************************************************/
 /*                                                     */
 /*                                                     */
 /*                                                     */
 /*******************************************************/
void AdvanceOneMinute(void) {
   //for (int x = 0; x < 410; x++){
   /*   currentMillis = micros();
      if(currentMillis-last_time >= 1000){
         DoStep(410); 
         time = time + micros() - last_time;
         last_time = micros();
      }*/
   //}
   motor.step(-4096);
   motor.stop();
   
   /*
   //this method makes the motor rotate a given number of degrees, it works with float
   //you can give angles like 90.5, but you can't give negative values, it rotates to the direction currently set
   //stepper.rotateDegrees(135);
   stepper.rotate(1);
  
   while (!stepper.isDone()){
      stepper.run();
   }
   //SetTimeToZero();
  */
}
 /*******************************************************/
 /*                                                     */
 /*                                                     */
 /*                                                     */
 /*******************************************************/
/*void DoStep(int xw){
   for (int x = 0; x < xw; x++){
      switch(Steps){
         case 0:
            digitalWrite(IN1, LOW); 
            digitalWrite(IN2, LOW);
            digitalWrite(IN3, LOW);
            digitalWrite(IN4, HIGH);
            break; 
         case 1:
            digitalWrite(IN1, LOW); 
            digitalWrite(IN2, LOW);
            digitalWrite(IN3, HIGH);
            digitalWrite(IN4, HIGH);
            break; 
         case 2:
            digitalWrite(IN1, LOW); 
            digitalWrite(IN2, LOW);
            digitalWrite(IN3, HIGH);
            digitalWrite(IN4, LOW);
            break; 
         case 3:
            digitalWrite(IN1, LOW); 
            digitalWrite(IN2, HIGH);
            digitalWrite(IN3, HIGH);
            digitalWrite(IN4, LOW);
            break; 
         case 4:
            digitalWrite(IN1, LOW); 
            digitalWrite(IN2, HIGH);
            digitalWrite(IN3, LOW);
            digitalWrite(IN4, LOW);
            break; 
         case 5:
            digitalWrite(IN1, HIGH); 
            digitalWrite(IN2, HIGH);
            digitalWrite(IN3, LOW);
            digitalWrite(IN4, LOW);
            break; 
         case 6:
            digitalWrite(IN1, HIGH); 
            digitalWrite(IN2, LOW);
            digitalWrite(IN3, LOW);
            digitalWrite(IN4, LOW);
            break; 
         case 7:
            digitalWrite(IN1, HIGH); 
            digitalWrite(IN2, LOW);
            digitalWrite(IN3, LOW);
            digitalWrite(IN4, HIGH);
            break; 
         default:
            digitalWrite(IN1, LOW); 
            digitalWrite(IN2, LOW);
            digitalWrite(IN3, LOW);
            digitalWrite(IN4, LOW);
            break; 
      } //Switch
   SetDirection();
   } // for
} */
 /*******************************************************/
 /*                                                     */
 /*                                                     */
 /*                                                     */
 /*******************************************************/
/*void SetDirection(){
   if(Direction == 1) { Steps++; }
   if(Direction == 0) { Steps--; }
   if(Steps     >  7) { Steps = 0;}
   if(Steps     <  0) { Steps = 7;}
}*/

