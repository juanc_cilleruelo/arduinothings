//Arduino Dice made by Juan C.Cilleruelo 

const int LED_1_01  =  9;
const int LED_1_02  = 10;
const int LED_1_03  = 11;
const int LED_1_04  =  8;
const int LED_1_05  =  7;
const int LED_1_06  =  6;
const int LED_1_07  =  5;

const int LED_2_01  =  2;
const int LED_2_02  =  3;
const int LED_2_03  =  4;
const int LED_2_04  =  1;
const int LED_2_05  = 13;
const int LED_2_06  =  0;
const int LED_2_07  = 12;

const int BUTTON    = A0;

const int LED   = 4;
      int Value = 0;
      int state = 0;

void setup(){
  pinMode(LED_1_01, OUTPUT);
  pinMode(LED_1_02, OUTPUT);
  pinMode(LED_1_03, OUTPUT);
  pinMode(LED_1_04, OUTPUT);
  pinMode(LED_1_05, OUTPUT);
  pinMode(LED_1_06, OUTPUT);
  pinMode(LED_1_07, OUTPUT);
  
  pinMode(LED_2_01, OUTPUT);
  pinMode(LED_2_02, OUTPUT);
  pinMode(LED_2_03, OUTPUT);
  pinMode(LED_2_04, OUTPUT);
  pinMode(LED_2_05, OUTPUT);
  pinMode(LED_2_06, OUTPUT);
  pinMode(LED_2_07, OUTPUT);
  
  pinMode(BUTTON, INPUT);
  
  pinMode(LED   , OUTPUT);
  randomSeed(analogRead(0));
}

void loop(){
   Value = digitalRead(BUTTON);
   if (Value == HIGH){
     
      delay(100);
   
      ClearPreviousValuesOnBothDices();
      delay(500);
        
      ShowResult_Dice_1(random(1, 7));
      
      ShowResult_Dice_2(random(1, 7));
   }     

}

/*======================================================*/
void ShowResult_Dice_1(long Number)
{
  switch(Number)
      {
         case 1: SetDice1_Number_One();        
         break;
         case 2: SetDice1_Number_Two();
         break;
         case 3: SetDice1_Number_Three();
         break;
         case 4: SetDice1_Number_Four();
         break;
         case 5: SetDice1_Number_Five();
         break;
         case 6: SetDice1_Number_Six();
         break;
         default:
         break;
      }
}
/*======================================================*/
void ShowResult_Dice_2(long Number)
{
  switch(Number)
      {
         case 1: SetDice2_Number_One();        
         break;
         case 2: SetDice2_Number_Two();
         break;
         case 3: SetDice2_Number_Three();
         break;
         case 4: SetDice2_Number_Four();
         break;
         case 5: SetDice2_Number_Five();
         break;
         case 6: SetDice2_Number_Six();
         break;
         default:
         break;
      }
}
/*======================================================*/
void ClearPreviousValuesOnBothDices()
{
   digitalWrite(LED_1_01, LOW);
   digitalWrite(LED_1_02, LOW);
   digitalWrite(LED_1_03, LOW);
   digitalWrite(LED_1_04, LOW);
   digitalWrite(LED_1_05, LOW);
   digitalWrite(LED_1_06, LOW);
   digitalWrite(LED_1_07, LOW);
   
   digitalWrite(LED_2_01, LOW);
   digitalWrite(LED_2_02, LOW);
   digitalWrite(LED_2_03, LOW);
   digitalWrite(LED_2_04, LOW);
   digitalWrite(LED_2_05, LOW);
   digitalWrite(LED_2_06, LOW);
   digitalWrite(LED_2_07, LOW);
   
   /* Test All Numbers */
   /*ShowResult_Dice_1(1);   delay(500); SetDice1_Clear();
   ShowResult_Dice_1(2);   delay(500); SetDice1_Clear();
   ShowResult_Dice_1(3);   delay(500); SetDice1_Clear();
   ShowResult_Dice_1(4);   delay(500); SetDice1_Clear();
   ShowResult_Dice_1(5);   delay(500); SetDice1_Clear();
   ShowResult_Dice_1(6);   delay(500); SetDice1_Clear();
   
   ShowResult_Dice_2(1);   delay(500); SetDice2_Clear();
   ShowResult_Dice_2(2);   delay(500); SetDice2_Clear();
   ShowResult_Dice_2(3);   delay(500); SetDice2_Clear();
   ShowResult_Dice_2(4);   delay(500); SetDice2_Clear();
   ShowResult_Dice_2(5);   delay(500); SetDice2_Clear();
   ShowResult_Dice_2(6);   delay(500); SetDice2_Clear();
   */  
      
   /* Test del orden de los leds */
   /*delay(500);
   digitalWrite(LED_1_01, HIGH); delay(500);
   digitalWrite(LED_1_02, HIGH); delay(500);
   digitalWrite(LED_1_03, HIGH); delay(500);
   digitalWrite(LED_1_04, HIGH); delay(500);
   digitalWrite(LED_1_05, HIGH); delay(500);
   digitalWrite(LED_1_06, HIGH); delay(500);
   digitalWrite(LED_1_07, HIGH); delay(500);
   
   digitalWrite(LED_2_01, HIGH); delay(500);
   digitalWrite(LED_2_02, HIGH); delay(500);
   digitalWrite(LED_2_03, HIGH); delay(500);
   digitalWrite(LED_2_04, HIGH); delay(500);
   digitalWrite(LED_2_05, HIGH); delay(500);
   digitalWrite(LED_2_06, HIGH); delay(500);
   digitalWrite(LED_2_07, HIGH); delay(500); */
  
}

void SetDice1_Number_One()
{
   digitalWrite(LED_1_04, HIGH);
}

void SetDice1_Number_Two()
{
   digitalWrite(LED_1_01, HIGH);
   digitalWrite(LED_1_07, HIGH);
}

void SetDice1_Number_Three()
{
   digitalWrite(LED_1_01, HIGH);
   digitalWrite(LED_1_04, HIGH);
   digitalWrite(LED_1_07, HIGH);
}

void SetDice1_Number_Four()
{
   digitalWrite(LED_1_01, HIGH);
   digitalWrite(LED_1_03, HIGH);
   digitalWrite(LED_1_05, HIGH);
   digitalWrite(LED_1_07, HIGH);
}

void SetDice1_Number_Five()
{
   digitalWrite(LED_1_01, HIGH);
   digitalWrite(LED_1_03, HIGH);
   digitalWrite(LED_1_04, HIGH);
   digitalWrite(LED_1_05, HIGH);
   digitalWrite(LED_1_07, HIGH);
}

void SetDice1_Number_Six()
{
   digitalWrite(LED_1_01, HIGH);
   digitalWrite(LED_1_02, HIGH);
   digitalWrite(LED_1_03, HIGH);
   digitalWrite(LED_1_05, HIGH);
   digitalWrite(LED_1_06, HIGH);
   digitalWrite(LED_1_07, HIGH);
}

void SetDice1_Clear()
{
   digitalWrite(LED_1_01, LOW);
   digitalWrite(LED_1_02, LOW);
   digitalWrite(LED_1_03, LOW);
   digitalWrite(LED_1_04, LOW);
   digitalWrite(LED_1_05, LOW);
   digitalWrite(LED_1_06, LOW);
   digitalWrite(LED_1_07, LOW);
}

/*======================================*/

void SetDice2_Number_One()
{
   digitalWrite(LED_2_04, HIGH);
}

void SetDice2_Number_Two()
{
   digitalWrite(LED_2_01, HIGH);
   digitalWrite(LED_2_07, HIGH);
}

void SetDice2_Number_Three()
{
   digitalWrite(LED_2_01, HIGH);
   digitalWrite(LED_2_04, HIGH);
   digitalWrite(LED_2_07, HIGH);
}

void SetDice2_Number_Four()
{
   digitalWrite(LED_2_01, HIGH);
   digitalWrite(LED_2_03, HIGH);
   digitalWrite(LED_2_05, HIGH);
   digitalWrite(LED_2_07, HIGH);
}


void SetDice2_Number_Five()
{
   digitalWrite(LED_2_01, HIGH);
   digitalWrite(LED_2_03, HIGH);
   digitalWrite(LED_2_04, HIGH);
   digitalWrite(LED_2_05, HIGH);
   digitalWrite(LED_2_07, HIGH);
}

void SetDice2_Number_Six()
{
   digitalWrite(LED_2_01, HIGH);
   digitalWrite(LED_2_02, HIGH);
   digitalWrite(LED_2_03, HIGH);
   digitalWrite(LED_2_05, HIGH);
   digitalWrite(LED_2_06, HIGH);
   digitalWrite(LED_2_07, HIGH);
}

void SetDice2_Clear()
{
   digitalWrite(LED_2_01, LOW);
   digitalWrite(LED_2_02, LOW);
   digitalWrite(LED_2_03, LOW);
   digitalWrite(LED_2_04, LOW);
   digitalWrite(LED_2_05, LOW);
   digitalWrite(LED_2_06, LOW);
   digitalWrite(LED_2_07, LOW);
}

