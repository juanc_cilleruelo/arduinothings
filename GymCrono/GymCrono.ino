#define SPEAKER A0
#define DIGIT_1 A1
#define DIGIT_2 A2
#define DIGIT_3 A3
#define DIGIT_4 A4

#define MAX_DIGITS 4

#define BUTTON_PLUS_1   8
#define BUTTON_PLUS_5   9
#define BUTTON_PLUS_10 10
#define BUTTON_RESET   11
#define BUTTON_START   12

#define SEGMENT_A 0
#define SEGMENT_B 1
#define SEGMENT_C 2
#define SEGMENT_D 3
#define SEGMENT_E 4
#define SEGMENT_F 5
#define SEGMENT_G 6
#define SEGMENT_P 7  // Point

#define MAX_SEGMENTS 8// No segment.

byte Digit[10][8] =
   { /*a, b, c, d, e, f, g, p*/
     { 1, 1, 1, 1, 1, 1, 0, 0 },    // 0
     { 0, 1, 1, 0, 0, 0, 0, 0 },    // 1
     { 1, 1, 0, 1, 1, 0, 1, 0 },    // 2
     { 1, 1, 1, 1, 0, 0, 1, 0 },    // 3
     { 0, 1, 1, 0, 0, 1, 1, 0 },    // 4
     { 1, 0, 1, 1, 0, 1, 1, 0 },    // 5
     { 1, 0, 1, 1, 1, 1, 1, 0 },    // 6
     { 1, 1, 1, 0, 0, 0, 0, 0 },    // 7
     { 1, 1, 1, 1, 1, 1, 1, 0 },    // 8
     { 1, 1, 1, 1, 0, 1, 1, 0 }     // 9
   };


void setup() {
  pinMode(SPEAKER, OUTPUT);
  pinMode(DIGIT_1, OUTPUT);
  pinMode(DIGIT_2, OUTPUT);
  pinMode(DIGIT_3, OUTPUT);
  pinMode(DIGIT_4, OUTPUT);

  pinMode(BUTTON_PLUS_1 , INPUT);
  pinMode(BUTTON_PLUS_5 , INPUT);
  pinMode(BUTTON_PLUS_10, INPUT);
  pinMode(BUTTON_RESET  , INPUT);
  pinMode(BUTTON_START  , INPUT);

  pinMode(SEGMENT_A, OUTPUT);
  pinMode(SEGMENT_B, OUTPUT);
  pinMode(SEGMENT_C, OUTPUT);
  pinMode(SEGMENT_D, OUTPUT);
  pinMode(SEGMENT_E, OUTPUT);
  pinMode(SEGMENT_F, OUTPUT);
  pinMode(SEGMENT_G, OUTPUT);
  pinMode(SEGMENT_P, OUTPUT);

  /* Set all the outputs to LOW */
  digitalWrite(SPEAKER, LOW);
  digitalWrite(DIGIT_1, HIGH);
  digitalWrite(DIGIT_2, HIGH);
  digitalWrite(DIGIT_3, HIGH);
  digitalWrite(DIGIT_4, HIGH);

  digitalWrite(SEGMENT_A, LOW);
  digitalWrite(SEGMENT_B, LOW);
  digitalWrite(SEGMENT_C, LOW);
  digitalWrite(SEGMENT_D, LOW);
  digitalWrite(SEGMENT_E, LOW);
  digitalWrite(SEGMENT_F, LOW);
  digitalWrite(SEGMENT_G, LOW);
  digitalWrite(SEGMENT_P, LOW);

}

void loop() {
     for (int x = 1; x < MAX_DIGITS +1; x++){
        TestDisplay(x);
        delay(200);
        
        for (int i = 0; i < 10; i++){
           Display(x, i);
           delay(200);
        }
        delay(200);
     }
     delay(200);

     
}


void Display(int Position, int Number)
{  
   switch(Position){
     case 1:
        digitalWrite(DIGIT_1, LOW);
        digitalWrite(DIGIT_2, HIGH);
        digitalWrite(DIGIT_3, HIGH);
        digitalWrite(DIGIT_4, HIGH);      
        break;
     case 2:
        digitalWrite(DIGIT_1, HIGH);
        digitalWrite(DIGIT_2, LOW );
        digitalWrite(DIGIT_3, HIGH);
        digitalWrite(DIGIT_4, HIGH);             
        break;
     case 3:
        digitalWrite(DIGIT_1, HIGH);
        digitalWrite(DIGIT_2, HIGH);
        digitalWrite(DIGIT_3, LOW );
        digitalWrite(DIGIT_4, HIGH);      
        break;
     case 4:
        digitalWrite(DIGIT_1, HIGH);
        digitalWrite(DIGIT_2, HIGH);
        digitalWrite(DIGIT_3, HIGH);
        digitalWrite(DIGIT_4, LOW );      
        break;
   }
 
   for (int i = SEGMENT_A; i < MAX_SEGMENTS; i++){    // Esto no cambia de la session anterior
      digitalWrite(i , Digit[Number][i]);
   }
}

void TestDisplay(int Position)
{  
   switch(Position){
     case 1:
        digitalWrite(DIGIT_1, LOW);
        digitalWrite(DIGIT_2, HIGH);
        digitalWrite(DIGIT_3, HIGH);
        digitalWrite(DIGIT_4, HIGH);      
        break;
     case 2:
        digitalWrite(DIGIT_1, HIGH);
        digitalWrite(DIGIT_2, LOW );
        digitalWrite(DIGIT_3, HIGH);
        digitalWrite(DIGIT_4, HIGH);             
        break;
     case 3:
        digitalWrite(DIGIT_1, HIGH);
        digitalWrite(DIGIT_2, HIGH);
        digitalWrite(DIGIT_3, LOW );
        digitalWrite(DIGIT_4, HIGH);      
        break;
     case 4:
        digitalWrite(DIGIT_1, HIGH);
        digitalWrite(DIGIT_2, HIGH);
        digitalWrite(DIGIT_3, HIGH);
        digitalWrite(DIGIT_4, LOW );      
        break;
   }

   // First turn off all segments
   for (int j = SEGMENT_A; j < (SEGMENT_P + 1); j++){
      digitalWrite(j, LOW);
   }

   //Sequentially turn on and turn off each segment 
   for (int j = SEGMENT_A; j < (SEGMENT_P + 1); j++){
      digitalWrite(j, HIGH);
      delay(100);
      digitalWrite(j, LOW);
      delay(100);
   }
   delay(200);

   //Sequentially turn on each segment
   for (int j = SEGMENT_A; j < (SEGMENT_P + 1); j++){
      digitalWrite(j, HIGH);
      delay(100);
   }
   delay(200);

   //Sequentially turn off each segment
   for (int j = SEGMENT_A; j < (SEGMENT_P + 1); j++){
      digitalWrite(j, LOW);
      delay(100);
   }
   delay(200);
}
