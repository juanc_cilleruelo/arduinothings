
// Hace un eco con el puerto serie del Arduino, leyendo una cadena completa

//#include "serial.h"
//#include <dynmem.h>

#include <LiquidCrystal.h>
LiquidCrystal lcd(8, 9, 2, 3, 4, 5); 

#define MAX_BUFFER 100

int STATUS_LED = 13;
int TRANSF_LED = 2;
int BLINK_DELAY = 2500;

/* Variables Globales */
int estado        = LOW;
int estado_transf = LOW;

unsigned long momento_anterior = 0;
unsigned long bytes_recibidos  = 0;


void setup(){
  Serial.begin(9600);
  // Queremos que un led parpadee mientras trabajamos
  pinMode(STATUS_LED, OUTPUT);
  // Queremos salida por el led de transferencia
  pinMode(TRANSF_LED, OUTPUT);
}

/*=====================================================================================================================*/

/*int serialGetString(char *string, int max)
{
  unsigned i = 0;
  char     sIn;
  // La cadena va desde 0 a max-2. El ultimo caracter (max-1) es el indicador de fin de cadena \0
  --max;       
  while (Serial.available() && i < max)
    {
      sIn         = Serial.read();
      string[i++] = sIn;
      //Damos unos instantes al siguiente caracter
      delayMicroseconds(500);
    }
  string[i++] = '\0';
  return i;
}*/

/*=====================================================================================================================*/
int serialGetString(char *string, int max)
{
  unsigned i=0;
  char sIn;
  unsigned long m;
  // Queremos que la cadena se rellene hasta max-2 para que en el carácter
  // max-1 (el último) podamos meter el terminador \0
  --max;       
  while (Serial.available() && i<max)
    {
      sIn=Serial.read();
      string[i++]=sIn;
      m=millis();
      while (!Serial.available() || millis()>=m+1);
    }
  string[i++]='\0';
  return i;
}
/*=====================================================================================================================*/

void loop ()
{  
  int           recibe;
  unsigned long momento_actual = millis();
  char          buf[MAX_BUFFER];

  // No bloqueante, si hay algo para leer entramos, si no, no.
  if(Serial.available())
    {
      serialGetString(buf, MAX_BUFFER);
      // Escribimos el buffer completo
      Serial.println((char*)buf);
      //lcd.println((char*)buf);
    }
    
  // No usamos delay para el parpadeo porque nos entorpece la comunicación con el serial
  if (momento_actual - momento_anterior >= BLINK_DELAY)
    {
      // Cambiamos el estado siguiente. Si era HIGH (1) ahora será
      // LOW (0). He leído en algún lado que el valor de HIGH no
      // siempre es 1; pero en este caso sí es así.
      estado=!estado;
      // Escribimos el estado actual del led
      digitalWrite(STATUS_LED, estado);
      // Establecemos el momento anterior como actual.
      momento_anterior = momento_actual;
    }
}
